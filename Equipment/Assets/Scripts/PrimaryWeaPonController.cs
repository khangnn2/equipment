using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PrimaryWeaPonController : Singleton<PrimaryWeaPonController>
{
    public GameObject pnlNotBought;
    public Image ImageGun;
    private void Start()
    {
        pnlNotBought.SetActive(false);
    }
    public void Use()
    {

        for (int i = 0; i < ShopGunController.Ins.listbtnGun.Count; i++)
        {
            if (ShopGunController.Ins.listbtnGun[i].isSelected)
            {
                bool isUnblock = Pref.GetBool(PrefConst.id_Gun + ShopGunController.Ins.listbtnGun[i].gun.idGun);
                if (isUnblock)
                {
                    PlayerPrefs.SetInt(PrefConst.cur_Gun, ShopGunController.Ins.listbtnGun[i].gun.idGun);
                }
                else
                {
                    pnlNotBought.SetActive(true);
                }
            }
        }
        ShopGunController.Ins.UpdateListBtnGun();
    }
    public void RentOut()
    {
        for (int i = 0; i < ShopGunController.Ins.listbtnGun.Count; i++)
        {
            if (ShopGunController.Ins.listbtnGun[i].isSelected)
            {
                Pref.setBool(PrefConst.id_Gun + ShopGunController.Ins.listbtnGun[i].gun.idGun, true);
            }

        }
        ShopGunController.Ins.UpdateListBtnGun();
    }
    public void Resume()
    {
        pnlNotBought.SetActive(false);
    }
}
