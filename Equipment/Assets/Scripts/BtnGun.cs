using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BtnGun : MonoBehaviour
{
    public Image icon;
    public Text text;
    public Gun gun;
    public GameObject effectSelected;
    public bool isSelected=false;
    private void Start()
    {
        effectSelected.SetActive(false);

    }
    public void Selected()
    {
        for(int i=0;i<ShopGunController.Ins.listbtnGun.Count;i++)
        {
            ShopGunController.Ins.listbtnGun[i].effectSelected.SetActive(false);
            ShopGunController.Ins.listbtnGun[i].isSelected = false;
        }
        effectSelected.SetActive(true);
        isSelected = true;
        PlasmaGunController.Ins.ShowInfoGun(gun);
        

    }
    public void UpdateBtnGun(Gun g)
    {
        gun = g;
        icon.sprite = g.icon;
        bool isUnblock = Pref.GetBool(PrefConst.id_Gun + g.idGun);
        if(isUnblock)
        {
            if(PlayerPrefs.GetInt(PrefConst.cur_Gun)==g.idGun)
            {
                text.text = "Used";
                text.color = new Color(0.16f, 0.75f, 0.1f);

            }
            else
            {
                text.text = "Rended Out";
                text.color= new Color(0.9f, 0.65f, 0.27f);
            }
        }
        else
        {
            text.text = "";
        }
    }
}
