using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pref : MonoBehaviour
{
    private static int curGun;
    public static int CurGun { get => PlayerPrefs.GetInt(PrefConst.cur_Gun); set => PlayerPrefs.SetInt(PrefConst.cur_Gun,value); }
    public static void setBool(string key, bool isOn)
    {
        if(isOn)
        {
            PlayerPrefs.SetInt(key, 1);
        }
        else
        {
            PlayerPrefs.SetInt(key, 0);
        }
    }
    public static bool GetBool(string key)
    {
        if(PlayerPrefs.GetInt(key)==1)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
