using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopGunController : Singleton<ShopGunController>
{
    public GameObject buttonGun;
    public GameObject gridLayout;
    public Gun[] listGun;

    public List<BtnGun> listbtnGun;
    public Scrollbar sc;
    // Start is called before the first frame update
    void Start()
    {
        sc.size = 0f;
        for (int i = 0; i < listGun.Length; i++)
        {
            GameObject g = Instantiate(buttonGun, Vector3.zero, Quaternion.identity) as GameObject;
            g.transform.SetParent(gridLayout.transform);
            g.transform.position = Vector3.zero;
            g.transform.localScale = Vector3.one;
            g.GetComponent<BtnGun>().UpdateBtnGun(listGun[i]);
            if (i == 0)
            {
                Pref.setBool(PrefConst.id_Gun + g.GetComponent<BtnGun>().gun.idGun, true);
            }
            else
            {
                if (!PlayerPrefs.HasKey(PrefConst.id_Gun + g.GetComponent<BtnGun>().gun.idGun))
                {
                    Pref.setBool(PrefConst.id_Gun + g.GetComponent<BtnGun>().gun.idGun, false);
                }
            }
            listbtnGun.Add(g.GetComponent<BtnGun>());

        }
        init();
    }
    public void init()
    {
        for (int i = 0; i < listbtnGun.Count; i++)
        {
            if (listbtnGun[i].text.text == "Used")
            {

                PrimaryWeaPonController.Ins.ImageGun.sprite = listbtnGun[i].icon.sprite;
                PlasmaGunController.Ins.ShowInfoGun(listbtnGun[i].gun);
            }
        }
    }
    public void UpdateListBtnGun()
    {
        for (int i = 0; i < listbtnGun.Count; i++)
        {
            listbtnGun[i].UpdateBtnGun(listbtnGun[i].gun);

        }
    }
}
