using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlasmaGunController : Singleton<PlasmaGunController>
{
    public GameObject pnlUpgrade;
    public GameObject pnlSuccessUpgrade;
    public Text txtDame;
    public Text txtDispersion;
    public Text txtRateOfFire;
    public Text txtReloadSpeed;
    public Text txtAmmunition;
    // Start is called before the first frame update
    void Start()
    {
        
    }
    public void ShowInfoGun(Gun g)
    {
        txtDame.text = g.damage.ToString();
        txtDispersion.text = g.dispersion.ToString();
        txtRateOfFire.text = g.rateOfFire.ToString() + " RPM";
        txtReloadSpeed.text = g.reloadSpeed.ToString() + "%";
        txtAmmunition.text = g.ammunition.ToString() + "/100";
    }
    public void SelectUpgrade()
    {
        pnlUpgrade.SetActive(true);
    }
    public void AcceptUpgrade()
    {
        Upgrade();
        pnlUpgrade.SetActive(false);
        pnlSuccessUpgrade.SetActive(true);
    }
    public void Upgrade()
    {
        for (int i = 0; i <ShopGunController.Ins.listbtnGun.Count; i++)
        {
            if (ShopGunController.Ins.listbtnGun[i].isSelected)
            {
                ShopGunController.Ins.listbtnGun[i].gun.damage += 25;
                ShopGunController.Ins.listbtnGun[i].gun.dispersion += 10;
                ShopGunController.Ins.listbtnGun[i].gun.rateOfFire += 100;
                ShopGunController.Ins.listbtnGun[i].gun.reloadSpeed += 10;
                if (ShopGunController.Ins.listbtnGun[i].gun.reloadSpeed >= 100)
                {
                    ShopGunController.Ins.listbtnGun[i].gun.reloadSpeed = 100;
                }
                ShopGunController.Ins.listbtnGun[i].gun.ammunition += 20;
                if (ShopGunController.Ins.listbtnGun[i].gun.ammunition >= 100)
                {
                    ShopGunController.Ins.listbtnGun[i].gun.ammunition = 100;
                }
                ShowInfoGun(ShopGunController.Ins.listbtnGun[i].gun);
            }
        }
    }
    public void Resume()
    {
        pnlUpgrade.SetActive(false);
        pnlSuccessUpgrade.SetActive(false);
    }
}
